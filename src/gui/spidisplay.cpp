#include "spidisplay.h"
#ifdef USE_SPI_HDMI

/* Null, because instance will be initialized on demand. */
SPIDisplay* SPIDisplay::spiDisplayInstance = 0;

SPIDisplay* SPIDisplay::getDisplay()
{
    if (spiDisplayInstance == 0)
    {
        spiDisplayInstance = new SPIDisplay();
        printf("Size of data is %d bits\n",8*(2*sizeof(uint16_t)+sizeof(lv_color_t)));
    }

    return spiDisplayInstance;
}


SPIDisplay::SPIDisplay(){    
  Serial.println("LVGL configuring");

  locked = true;

  lv_disp_draw_buf_init(&disp_buf, buf, NULL, LV_HOR_RES_MAX * 10);
  //Initialize the display
  lv_disp_drv_init(&disp_drv);
  disp_drv.hor_res = 200;
  disp_drv.ver_res = 200;
  disp_drv.flush_cb = my_disp_flush;
  disp_drv.draw_buf = &disp_buf;
  lv_disp_drv_register(&disp_drv);

  Serial.println("driver regisgtered");
  
  pinMode(SS, OUTPUT); //VSPI SS
  WRITEPIN(SS, HIGH);
  Serial.println("SPI created");
}


/* Display flushing */
void SPIDisplay::my_disp_flush(lv_disp_drv_t *disp, const lv_area_t *area, lv_color_t *color_p)
{
  uint16_t x, y;  

  SPISingleton* spiInstance = SPISingleton::getInstance();
  spiInstance->beginTransaction(SPISettings(SPICLK, MSBFIRST, SPI_MODE0));

  unsigned char myData[6];
  for (y = area->y1; y <= area->y2; y++)
  {
    for (x = area->x1; x <= area->x2; x++)
    {       
      myData[5]= color_p->full >> 8;
      myData[4]= color_p->full;
      myData[3]= y;
      myData[2]= y >> 8;
      myData[1]= x;
      myData[0]= x >> 8;
      WRITEPIN(SS, LOW); //pull SS slow to prep other end for transfer  
      spiInstance->transfer(myData,6); //spiInstance->transfer(display->spiData->y);
      WRITEPIN(SS, HIGH); //pull ss high to signify end of data transfer

      ++color_p;      
    }
  }
  spiInstance->endTransaction();

  lv_disp_flush_ready(disp);  
}

/* Interrupt driven periodic handler */
void SPIDisplay::lv_tick_handler(void)
{
  //lv_tick_inc(LVGL_TICK_PERIOD);
}

/* Reading input device (simulated encoder here) */
bool SPIDisplay::read_encoder(lv_indev_drv_t * indev, lv_indev_data_t * data)
{
  static int32_t last_diff = 0;
  int32_t diff = 0; /* Dummy - no movement */
  int btn_state = LV_INDEV_STATE_REL; /* Dummy - no press */

  data->enc_diff = diff - last_diff;;
  data->state = LV_INDEV_STATE_RELEASED;

  last_diff = diff;

  return false;
}
#endif
#include "filelist.h"

/*
static void event_handler(lv_obj_t * obj, lv_event_t event)
{
  if (event == LV_EVENT_CLICKED) {
    printf("Clicked: %s\n", lv_list_get_btn_text(obj));
  }
}*/

FileList::FileList()
{
  /*Create a list*/
  if (lv_scr_act()){
    list  = lv_list_create(lv_scr_act());
    lv_obj_set_size(list, lv_disp_get_hor_res(NULL), lv_disp_get_ver_res(NULL));
    lv_obj_align(list, LV_ALIGN_CENTER, 0, 0);
  }  
}

void FileList::listPreviousItem() {
 if(currentItem != NULL) {
  lv_obj_clear_state(currentItem,LV_STATE_FOCUS_KEY);
  const uint32_t index = lv_obj_get_index(currentItem);
  if (index>0){
    currentItem = lv_obj_get_child(list, index - 1);
    if (currentItem != NULL){
      lv_obj_scroll_to_view(currentItem, LV_ANIM_ON);
    }else{
        currentItem = lv_obj_get_child(list, index);
    }
  }
 }
 lv_obj_add_state(currentItem, LV_STATE_FOCUS_KEY);
}

void FileList::listNextItem() {
 if(currentItem!= NULL){
    lv_obj_clear_state(currentItem,LV_STATE_FOCUS_KEY);
    const uint32_t index = lv_obj_get_index(currentItem);
    currentItem = lv_obj_get_child(list, index + 1);
    if (currentItem != NULL){
      lv_obj_scroll_to_view(currentItem, LV_ANIM_ON);
    }else{
      currentItem = lv_obj_get_child(list, index);
    }
    lv_obj_add_state(currentItem, LV_STATE_FOCUS_KEY);
 }
}

void FileList::listAddItem( const char* fileName){
  lv_obj_t * list_btn;
  list_btn = lv_list_add_btn(list, LV_SYMBOL_FILE, fileName);
  //lv_obj_set_event_cb(list_btn, event_handler);  
}

void FileList::listAddDirectory(const char* dirName){
  lv_obj_t * list_btn;
  list_btn = lv_list_add_btn(list, LV_SYMBOL_DIRECTORY, dirName);
  //lv_obj_set_event_cb(list_btn, event_handler);  
}

void FileList::listSelectTopItem(){
  currentItem = lv_obj_get_child(list, 0);
  lv_obj_add_state(currentItem, LV_STATE_FOCUS_KEY);
}

const char *FileList::getCurrentItemText(){
  uint32_t i;
  for(i = 0; i < lv_obj_get_child_cnt(list); i++) {
      lv_obj_t * child = lv_obj_get_child(list, i);
      if(lv_obj_has_state(child, LV_STATE_FOCUS_KEY)){
        currentItem=child;
      }
  }
  return lv_list_get_btn_text(list,currentItem);
}

void FileList::show(){
  lv_obj_clear_flag(list, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(list, false);
}

void FileList::hide(){
  lv_obj_add_flag(list, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(list, true);
}
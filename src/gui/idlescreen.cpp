#include "idlescreen.h"

lv_obj_t * imgHummingbird = 0;

LV_IMG_DECLARE(logo_hummingbird);

IdleScreen::IdleScreen(){
  if (lv_scr_act()){
    imgHummingbird = lv_img_create(lv_scr_act());
    lv_img_set_src(imgHummingbird, &logo_hummingbird);
    lv_obj_align(imgHummingbird,LV_ALIGN_CENTER, 0, 0);
  }
}

void IdleScreen::guiShowIdleScreen(){
  lv_obj_clear_flag(imgHummingbird, LV_OBJ_FLAG_HIDDEN);
}

void IdleScreen::guiHideIdleScreen(){
  lv_obj_add_flag(imgHummingbird, LV_OBJ_FLAG_HIDDEN);
}

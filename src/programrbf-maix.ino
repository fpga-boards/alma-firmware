/**
  BSD 3-Clause License

  Copyright (c) 2021, Guillermo Amat
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

  2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

  3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

//
// programrbf
// Maix Bit version by Guillermo Amat. Thanks to Victor Truco, Carlos Palmero
// and Marcus Andrade (bogermann) for the original sources
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#include <Shell.h>

#include <SPI.h>
#include <SdFat.h>
#include <sdios.h>

#include "jtag.h"
#include "shellcommands.h"
#include "gui/maixgui.h"
#include "gui/spisingleton.h"
#include "input/joystick.h"
#include "maixbutton.h"
#include "browser.h"

#ifdef K210
#define SD_SPI_CS 29
#endif
#ifdef ESP32
//#define SPI_DRIVER_SELECT 1 // This should force the use of HSPI on SDFAT
#define SD_SPI_CS 5 // ESP32 VSPI_CS
#endif

#ifdef RASPBERRY_PI_PICO
#define SD_SPI_CS 17
#include "pico-config.h"
#endif

// SD_FAT_TYPE = 0 for SdFat/File as defined in SdFatConfig.h,
// 1 for FAT16/FAT32, 2 for exFAT, 3 for FAT16/FAT32 and exFAT.
#define SD_FAT_TYPE 3
// Test with reduced SPI speed for breadboards.  SD_SCK_MHZ(4) will select
// the highest speed supported by the board that is not over 4 MHz.
// Change SPI_SPEED to SD_SCK_MHZ(50) for best performance.
#define SPI_SPEED SD_SCK_MHZ(4)
#if SD_FAT_TYPE == 0
SdFat SD;
#elif SD_FAT_TYPE == 1
SdFat32 SD;
#elif SD_FAT_TYPE == 2
SdExFat SD;
#elif SD_FAT_TYPE == 3
SdFs SD;
#else  // SD_FAT_TYPE
#error Invalid SD_FAT_TYPE
#endif  // SD_FAT_TYPE

#define MAX_INACTIVE_TIME 20000
unsigned int lastUserAction=millis();

Gui gui;
IdleScreen idleScreen;
FileList fileList;
Joystick joy;
SPISingleton *spiInstance;

void goToNextCore(void *, int val) {
  lastUserAction=millis();
  if (!lv_obj_has_flag(imgHummingbird, LV_OBJ_FLAG_HIDDEN)){ //if (!lv_obj_get_hidden(imgHummingbird)){ 
    lv_obj_clear_flag(container, LV_OBJ_FLAG_HIDDEN); //(lv_obj_set_hidden(container, false);
    fileList.show();
    idleScreen.guiHideIdleScreen();
  }
  if (fileList.visible()) {
    fileList.show();
    lv_obj_add_flag(labelResult, LV_OBJ_FLAG_HIDDEN);//lv_obj_set_hidden(labelResult, true);  

  } else {
    fileList.listNextItem();
  }
}

void goToPreviousCore(void *, int val) {
  fileList.listPreviousItem();
}

void updateProgressBar(int val) {
  lv_bar_set_value(barSendingUpdate, val, LV_ANIM_OFF);
  lv_event_send(barSendingUpdate, LV_EVENT_REFRESH, NULL);
  lv_task_handler();
  shell_printf("+");
}


void loadSelectedCore(void *, int val) {
  const char *coreName = fileList.getCurrentItemText();

  lastUserAction=millis();
  
  jtagSetup();
  if (jtagScan() == 0) {
    unsigned int startTime = millis();
    unsigned int duration = 0;
    char auxString[80];

    sprintf(auxString, "Loading core\n %s", coreName);

    lv_label_set_text(labelResult, auxString);
    fileList.hide();
    lv_obj_clear_flag(barSendingUpdate, LV_OBJ_FLAG_HIDDEN);
    lv_obj_clear_flag(labelResult, LV_OBJ_FLAG_HIDDEN);  

    if (jtagProgram(coreName) == SHELL_RET_SUCCESS) {
      duration = millis() - startTime;
      shell_printf("Programming time: %d ms\n", duration);
      sprintf(auxString, "Programming time: #3A8ABF %d# ms\nPress  #3A8ABF user button#  to close", duration);
      lv_label_set_text(labelResult, auxString);
      lv_obj_add_flag(barSendingUpdate, LV_OBJ_FLAG_HIDDEN); 
      lv_obj_clear_flag(labelResult, LV_OBJ_FLAG_HIDDEN); 

    } else {
      shell_printf("Something went wrong while programming the fpga\n");
    }
  }
  jtagRelease();
}

//! Process input signals from joystick and keyboard
void processInput(){
  spiInstance->beginTransaction(SPISettings(SPICLK, MSBFIRST, SPI_MODE0));
  WRITEPIN(SS, LOW); //pull SS slow to prep other end for transfer
  spiInstance->setData(spiInstance->transfer(0));
  WRITEPIN(SS, HIGH); //pull ss high to signify end of data transfer
  spiInstance->endTransaction();
  switch (joy.getStatus())
  {
  case JOY_UP: goToPreviousCore(NULL,1); break;
  case JOY_DOWN: goToNextCore(NULL,1); break;
  case JOY_BUTTON_A: browserOpenResource(NULL,2); break;
  
  default:
    break;
  }

}

void setup() {
  extern buttonObjectEvent onPressed;
  extern buttonObjectEvent onLongPressed;
  extern jtagEvent onValueChanged;

#ifdef PICO_SDK_VERSION_STRING
  set_sys_clock_khz(266000,true);
#endif

  // Prepare serial communication
  Serial.begin(115200);
  
  //Initialize lvgl
  gui.guiSetup();
  gui.guiShowError("GUI Ready");

  // Initialize the SD Library
  if (!SD.begin(SD_SPI_CS, SPI_SPEED)) {
    Serial.println("SD initialization failed!");
    gui.guiShowError("SD initialization failed!\nIs there an SD card inserted?");
    Serial.println(MISO);
    Serial.println(MOSI);
    Serial.println(SCK);
  }else {
    Serial.println("SD initialization done.");
  
    // Initialize command line interface (CLI)
    // We pass the function pointers to the read and write functions that we implement below
    // We can also pass a char pointer to display a custom start message
    shell_init(shell_reader, shell_writer, 0);
  
    // Add commands to the shell
    shell_register(command_idcode, PSTR("idcode"));
    shell_register(command_programrbf, PSTR("programrbf"));
  
  
    //Create GUI objectos/widgets
    gui.guiCreate();
    nodes=linkedListAddNode(nodes,"/");  
    browserShowDirectoryFiles("/");
  
    buttonSetup();
    onPressed = &goToNextCore;
    onLongPressed = &browserOpenResource;
    onValueChanged = &updateProgressBar;
    onFileOpen = &loadSelectedCore;  //File borwser event. See browser.h
  }
  spiInstance = SPISingleton::getInstance();
}

void loop() {  
  shell_task();
  if ((millis()-lastUserAction)>MAX_INACTIVE_TIME && lv_obj_has_flag(labelError,LV_OBJ_FLAG_HIDDEN)){
    fileList.hide();
    lv_obj_add_flag(container, LV_OBJ_FLAG_HIDDEN);
    idleScreen.guiShowIdleScreen();
    lastUserAction = millis();
  }
    
  processInput();

  lv_task_handler();
  //delay(5);

  // read the state of the pushbutton value:
  buttonEvent();
}

/**
   Function to read data from serial port
   Functions to read from physical media should use this prototype:
   int my_reader_function(char * data)
*/
int shell_reader(char *data) {
  // Wrapper for Serial.read() method
  if (Serial.available()) {
    *data = Serial.read();
    return 1;
  }
  return 0;
}

/**
   Function to write data to serial port
   Functions to write to physical media should use this prototype:
   void my_writer_function(char data)
*/
void shell_writer(char data) {
  // Wrapper for Serial.write() method
  Serial.write(data);
}

#include "joystick.h"

byte Joystick::getStatus(){
  byte data, returnValue=0;

  data= SPISingleton::getInstance()->getData();

  if (data!= lastInputCode){
    returnValue=lastInputCode;
    lastInputTime=millis();
    lastInputCode=data;
  }else if (data != JOY_BUTTON_A && (millis()-lastInputTime)>200){
    lastInputTime=millis();
    returnValue=lastInputCode;
  }
  return returnValue;
}

/**
BSD 3-Clause License

Copyright (c) 2021, Guillermo Amat
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/

#ifndef MAIX_JTAG_H
#define MAIX_JTAG_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <assert.h>

#define BUFSIZE 1000

#define MaxIR_ChainLength 100

#ifdef K210
//In the Maix Bit we will use the following pins: 18, 19 20, 21 (4, 17, 22, 27)
#define PIN_TCK     18
#define PIN_TDI     19
#define PIN_TMS     20
#define PIN_TDO     21
#define WRITEPIN digitalWrite
#endif
#ifdef ESP32 // Do not use 6-11, 34,35
#define PIN_TCK     32
#define PIN_TDI     25
#define PIN_TMS     26
#define PIN_TDO     33
#define WRITEPIN digitalWrite
#endif  
#ifdef RASPBERRY_PI_PICO
#define PIN_TCK     2
#define PIN_TDI     4
#define PIN_TMS     5
#define PIN_TDO     3
#define WRITEPIN gpio_put
#endif


#ifdef __cplusplus
extern "C" {
#endif

typedef void (*jtagEvent)(int);

void jtagSetup();
void jtagRelease();
int jtagScan();
int jtagProgram(const char* coreName);
int command_idcode(int argc, char** argv);
int command_programrbf(int argc, char** argv);

#ifdef __cplusplus
}
#endif

#endif //MAIX_JTAG_H
